﻿using UnityEngine;

public class CollisionDetection
{

    private LayerMask m_LayerMask;
    private Transform m_Player;

    private const float m_Inset = 0.025f;

    private int m_VerticalRayCount = 4, m_HorizontalRayCount = 4;
    private float m_VerticalRaySpacing, m_HorizontalRaySpacing;

    private RaycastPositions m_RayPosition;
    private CollisionFeedback m_CollisionFeedback;

    public CollisionFeedback CollisionFeedback
    {

        get { return m_CollisionFeedback; }
        
    }

    private BoxCollider2D m_Collider;

    public CollisionDetection(LayerMask layerMask, Transform player, int rayCound, BoxCollider2D collider)
    {

        m_LayerMask = layerMask;
        m_Player = player;
        m_VerticalRayCount = m_HorizontalRayCount = rayCound;
        m_Collider = collider;

        m_RayPosition = new RaycastPositions();
        m_CollisionFeedback = new CollisionFeedback();

    }

    public void Movement(Vector2 velocity)
    {

        RayCastPositions();
        m_CollisionFeedback.Reset();

        if(velocity.x != 0)
        {
            HorizontalCollision(ref velocity);
        }
   
        if(velocity.y != 0)
        {
            VerticalCollision(ref velocity);
        }

        m_Player.transform.Translate(velocity);

    }

    public void VerticalCollision(ref Vector2 velocity)
    {

        float verticalDirection = Mathf.Sign(velocity.y);
        float verticalRayLenght = Mathf.Abs(velocity.y) + m_Inset;

        Vector2 rayCastPosition = verticalDirection == -1 ? m_RayPosition.BottomLeft : m_RayPosition.TopLeft;

        for (int ray = 0; ray < m_VerticalRayCount; ray++)
        {

            RaycastHit2D rayCastHit = Physics2D.Raycast(rayCastPosition + Vector2.right * (m_VerticalRaySpacing * ray + velocity.x), verticalDirection * Vector3.up, verticalRayLenght, m_LayerMask);

            Debug.DrawRay(rayCastPosition + Vector2.right * (m_VerticalRaySpacing * ray + velocity.x), Vector3.up * verticalDirection * .5f, Color.red);

            if (rayCastHit)
            {

                velocity.y = (rayCastHit.distance - m_Inset) * verticalDirection;
                verticalRayLenght = rayCastHit.distance;

                m_CollisionFeedback.Below = verticalDirection == -1;
                m_CollisionFeedback.Above = verticalDirection == 1;

            }

        }

    }

    public void HorizontalCollision(ref Vector2 velocity)
    {

        float horizontalDirection = Mathf.Sign(velocity.x);
        float horizontalRayLenght = Mathf.Abs(velocity.x) + m_Inset;

        Vector2 rayCastPosition = horizontalDirection == -1 ? m_RayPosition.BottomLeft : m_RayPosition.BottomRight;

        for (int ray = 0; ray < m_HorizontalRayCount; ray++)
        {

            RaycastHit2D rayCastHit = Physics2D.Raycast(rayCastPosition + Vector2.up * m_HorizontalRaySpacing * ray, horizontalDirection * Vector3.right, horizontalRayLenght, m_LayerMask);

            Debug.DrawRay(rayCastPosition + Vector2.up * m_HorizontalRaySpacing * ray, Vector3.right * horizontalDirection * .5f, Color.red);

            if (rayCastHit)
            {

                velocity.x = (rayCastHit.distance - m_Inset) * horizontalDirection;
                horizontalRayLenght = rayCastHit.distance;

                m_CollisionFeedback.Left = horizontalDirection == -1;
                m_CollisionFeedback.Right = horizontalDirection == 1;

            }

        }

    }

    public void RayCastPositions()
    {

        Bounds bounds = m_Collider.bounds;
        bounds.Expand(m_Inset * -2f);

        m_RayPosition.BottomLeft = new Vector2(bounds.min.x, bounds.min.y);
        m_RayPosition.BottomRight = new Vector2(bounds.max.x, bounds.min.y);
        m_RayPosition.TopLeft = new Vector2(bounds.min.x, bounds.max.y);

        m_VerticalRaySpacing = (bounds.size.x) / (m_VerticalRayCount - 1);
        m_HorizontalRaySpacing = (bounds.size.y) / (m_HorizontalRayCount - 1);

    }
}
