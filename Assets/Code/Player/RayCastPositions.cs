﻿using UnityEngine;

public struct RaycastPositions
{

    private Vector2 m_BottomLeft,
                    m_BottomRight,
                    m_TopLeft;

    public Vector2 BottomLeft
    {

        get { return m_BottomLeft; }
        set { m_BottomLeft = value; }

    }

    public Vector2 BottomRight
    {

        get { return m_BottomRight; }
        set { m_BottomRight = value; }

    }

    public Vector2 TopLeft
    {

        get { return m_TopLeft; }
        set { m_TopLeft = value; }

    }


}