﻿using UnityEngine;

public struct CollisionFeedback
{

    private bool m_Above, m_Below;
    private bool m_Left, m_Right;

    public bool Above
    {

        get { return m_Above; }
        set { m_Above = value; }

    }

    public bool Below
    {

        get { return m_Below; }
        set { m_Below = value; }

    }

    public bool Left
    {

        get { return m_Left; }
        set { m_Left = value; }

    }

    public bool Right
    {

        get { return m_Right; }
        set { m_Right = value; }

    }

    public void Reset()
    {

        m_Above = m_Below = false;
        m_Left = m_Right = false;

    }
	
}
