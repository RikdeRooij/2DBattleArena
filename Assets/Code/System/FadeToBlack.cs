﻿using System.Collections;
using UnityEngine.UI;
using UnityEngine;

public class FadeToBlack : MonoBehaviour {


    [SerializeField] private Image m_FadeImage;                     
    [SerializeField] private Color m_FadeColor = Color.black;       
    [SerializeField] private float m_FadeDuration = 2.0f;           
    [SerializeField] private bool m_FadeInOnSceneLoad = false;      
    [SerializeField] private bool m_FadeInOnStart = false;          

    private bool m_IsFading;                                        
    private float m_FadeStartTime;                                  
    private Color m_FadeOutColor;                                   

    public bool IsFading { get { return m_IsFading; } }

    private void Awake()
    {

        m_FadeOutColor = new Color(m_FadeColor.r, m_FadeColor.g, m_FadeColor.b, 0f);
        m_FadeImage.enabled = true;

    }

    private void Start()
    {

        if (m_FadeInOnStart)
        {

            m_FadeImage.color = m_FadeColor;
            FadeIn();

        }

    }

    public void FadeOut()
    {

        FadeOut(m_FadeDuration);

    }


    public void FadeOut(float duration)
    {

        if (m_IsFading) return;

        StartCoroutine(BeginFade(m_FadeOutColor, m_FadeColor, duration));

    }

    public void FadeIn()
    {

        FadeIn(m_FadeDuration);

    }


    public void FadeIn(float duration)
    {

        if (m_IsFading) return;

        StartCoroutine(BeginFade(m_FadeColor, m_FadeOutColor, duration));

    }


    public IEnumerator BeginFadeOut()
    {

        yield return StartCoroutine(BeginFade(m_FadeOutColor, m_FadeColor, m_FadeDuration));

    }


    public IEnumerator BeginFadeOut(float duration)
    {

        yield return StartCoroutine(BeginFade(m_FadeOutColor, m_FadeColor, duration));

    }


    public IEnumerator BeginFadeIn()
    {

        yield return StartCoroutine(BeginFade(m_FadeColor, m_FadeOutColor, m_FadeDuration));

    }


    public IEnumerator BeginFadeIn(float duration)
    {

        yield return StartCoroutine(BeginFade(m_FadeColor, m_FadeOutColor, duration));

    }


    private IEnumerator BeginFade(Color startCol, Color endCol, float duration)
    {

        m_IsFading = true;

        float timer = 0f;

        while (timer <= duration)
        {

            m_FadeImage.color = Color.Lerp(startCol, endCol, timer / duration);

            timer += Time.deltaTime;

            yield return null;

        }

        m_IsFading = false;

    }

}
