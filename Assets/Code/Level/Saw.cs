﻿using System.Collections;
using UnityEngine;

public class Saw : LevelObstacle
{

    private float m_MovementSpeed = 1f,
                    m_MovementRange = 0.6f;


    protected override Collider2D GetTarget
    {

        get { return Physics2D.OverlapCircle(transform.position, 1f, m_LayerMask); }

    }

    protected override void Initialize(LayerMask layerMask)
    {

        base.Initialize(layerMask);

        StartCoroutine(MoveBlade());

    }

    private IEnumerator MoveBlade()
    {

        Vector3 sawPosition = Vector3.zero;

        while (m_GameIsRunning)
        {

            sawPosition.y = Mathf.Cos(Time.time * m_MovementSpeed) * m_MovementRange;

            transform.localPosition = sawPosition;

            yield return null;

        }

    }

}
