﻿// Based on script made by Mick Boere 2GD1


using UnityEngine;

[RequireComponent(typeof(Camera))]
public class MB_FollowPlayers : MonoBehaviour
{
    [SerializeField] private Transform[] m_Players;
    [SerializeField] private float m_DefaultCameraSize = 10f;
    [SerializeField] private float m_Multiplier = .2f;
    [SerializeField] private float m_MoveSpeed = 5f;
    [SerializeField] private float m_ZoomSmoothing = 5f;

    private Camera cam;

    void Start()
    {
        cam = gameObject.GetComponent<Camera>();
    }

    void Update()
    {

        Vector3 targetPosition = new Vector3();
        float longestDistance = 0f;

        for (int i = 0; i < m_Players.Length; i++)
        {
            targetPosition += m_Players[i].position;

            for (int j = 0; j < m_Players.Length; j++)
            {
                if (m_Players[i] != m_Players[j])
                {
                    float d = Vector3.Distance(m_Players[i].position, m_Players[j].position);

                    if (d > longestDistance)
                        longestDistance = d;
                }
            }
        }

        targetPosition /= m_Players.Length;
        targetPosition.z = -10;
        transform.position = Vector3.Lerp(transform.position, targetPosition, m_MoveSpeed * Time.deltaTime);
        cam.orthographicSize = Mathf.Lerp(cam.orthographicSize, m_DefaultCameraSize + longestDistance * m_Multiplier, m_ZoomSmoothing * Time.deltaTime);
    }
}